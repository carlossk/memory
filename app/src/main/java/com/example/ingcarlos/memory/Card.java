package com.example.ingcarlos.memory;

import android.widget.Button;

/**
 * Created by IngCarlos on 11/03/2015.
 */
public class Card {

    private Button btn;
    private int imgId;
    private boolean adiv;
    private boolean desc;

    public Card(Button btn,int imgId,boolean adiv,boolean desc) {

        this.btn = btn;
        this.imgId=imgId;
        this.adiv=adiv;
        this.desc=desc;

    }

    public int getImgId() {
        return imgId;
    }

    public void setImgId(int imgId) {
        this.imgId = imgId;
    }

    public boolean isDesc() {
        return desc;
    }

    public void setDesc(boolean desc) {
        this.desc = desc;
    }

    public Button getBtn() {
        return btn;
    }

    public void setBtn(Button btn) {
        this.btn = btn;
    }

    public boolean isAdiv() {
        return adiv;
    }

    public void setAdiv(boolean adiv) {
        this.adiv = adiv;
    }
}
