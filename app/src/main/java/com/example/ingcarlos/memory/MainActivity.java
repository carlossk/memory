package com.example.ingcarlos.memory;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.math.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Random;

public class MainActivity extends ActionBarActivity {
    private TextView txtV,txtRec;
    private Random rdm = new Random();
    private Card pCards[];
    private int [] imgs;
    private Button[] btns;
    private int pCont;
    private SharedPreferences shared;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        txtRec= (TextView) findViewById(R.id.txtRecord);
        //shared = this.getSharedPreferences("file",MODE_PRIVATE);
        //String record = shared.getString("record","1000");
        txtRec.setText("Último Récord: ????? ");

        pCont=0;
        txtV= (TextView) findViewById(R.id.txtCont);
        btns= new Button[]{
                (Button) findViewById(R.id.btn1),
                (Button) findViewById(R.id.btn2),
                (Button) findViewById(R.id.btn3),
                (Button) findViewById(R.id.btn4),
                (Button) findViewById(R.id.btn5),
                (Button) findViewById(R.id.btn6),
                (Button) findViewById(R.id.btn7),
                (Button) findViewById(R.id.btn8),
                (Button) findViewById(R.id.btn9),
                (Button) findViewById(R.id.btn10),
                (Button) findViewById(R.id.btn11),
                (Button) findViewById(R.id.btn12)
        };
        imgs = new int[]{
                R.drawable.uno1,
                R.drawable.dos,
                R.drawable.tres1,
                R.drawable.cuatro,
                R.drawable.cinco1,
                R.drawable.seis,
                R.drawable.siete,
                R.drawable.ocho,
                R.drawable.nueve,
                R.drawable.diez,
                R.drawable.once,
                R.drawable.doce,
                R.drawable.trece,
                R.drawable.catorce,
                R.drawable.diezises,
                R.drawable.diesisiete,
                R.drawable.diesiocho,
                R.drawable.diesinueve,
                R.drawable.veinte,

        };
        LinkedList al= aleatorio();
        pCards= new Card[12];
        for(int i=0; i<12;i++){
           pCards[i]= new Card(btns[i],imgs[((int) al.pop())],false,false);

        }

    }
public LinkedList aleatorio(){

    LinkedList<Integer> num = new LinkedList<>();

    int[] al1 = new int[6];
    LinkedList<Integer> al2 = new LinkedList<>();
    Aleatorios als1= new Aleatorios(0,19);
    Aleatorios als2= new Aleatorios(0,5);
    for(int i=0;i<6;i++) {
       al1[i]=als1.generar();
    }

    for(int i=0;i<6;i++) {
        al2.add(al1[als2.generar()]);
    }

    int flag=0;
    int j=0;
    for(int i=0;i<12;i++) {
        if(flag==0){
            num.add(al1[j]);
            flag=1;
            j++;
        }else{
            num.add(al2.pop());
            flag=0;
        }
        }

    return num;

}

public void OnClick(View v){
       switch (v.getId())
       {
           case R.id.btn1:
               PresButton(0);
               break;
           case R.id.btn2:
               PresButton(1);
               break;
           case R.id.btn3:
               PresButton(2);
               break;
           case R.id.btn4:
               PresButton(3);
               break;
           case R.id.btn5:
               PresButton(4);
               break;
           case R.id.btn6:
               PresButton(5);
               break;
           case R.id.btn7:
               PresButton(6);
               break;
           case R.id.btn8:
               PresButton(7);
               break;
           case R.id.btn9:
               PresButton(8);
               break;
           case R.id.btn10:
               PresButton(9);
               break;
           case R.id.btn11:
               PresButton(10);

               break;
           case R.id.btn12:
               PresButton(11);
               break;
       }

}
    public int VerificaC(int select){
        int pos=-1;
        for(int i=0;i<pCards.length;i++){
            Card c=pCards[i];
            if(c.isDesc() && !c.isAdiv() &&  i != select ){
                pos=i;
            }
        }

        return pos;
    }
    public void VerificaG(){
        int cont=0;
        for(int i=0; i<pCards.length;i++){

            if(pCards[i].isAdiv()){

                cont++;

            }
        }
        //txtRec.setText(""+cont);
        if(cont==12){
           // SaveRecord();
            mostrarAlertDialogo();

        }
    }
    private void mostrarAlertDialogo(){

        AlertDialog.Builder dialog = new AlertDialog.Builder(this);

        dialog.setMessage("GANASTE con "+pCont+" Intentos,\n Deseas Jugar Otra vez?");
        dialog.setCancelable(false);
        dialog.setPositiveButton("Si", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                Intent i = getBaseContext().getPackageManager()
                        .getLaunchIntentForPackage( getBaseContext().getPackageName() );
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                //MainActivity.this.finish();
            }
        });
        dialog.setNegativeButton("No", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        dialog.show();
    }
    private void PresButton(int btnID){
        btns[btnID].setBackgroundResource(pCards[btnID].getImgId());

        int poss=VerificaC(btnID);
        //txtV.setText(""+poss);
        if(poss>=0){
            if(pCards[btnID].getImgId()==pCards[poss].getImgId()){
                pCards[btnID].setAdiv(true);
                pCards[poss].setAdiv(true);

                pCont++;
                txtV.setText("Intento: "+pCont);

                btns[btnID].setEnabled(false);
                btns[poss].setEnabled(false);
                VerificaG();

            }else{

                btns[btnID].setBackgroundResource(R.drawable.portada1);
                btns[poss].setBackgroundResource(R.drawable.portada1);

                pCards[btnID].setDesc(false);
                pCards[poss].setDesc(false);

                pCont++;
                txtV.setText("Intento: "+pCont);
            }

        }else{

            pCards[btnID].setDesc(true);

        }
    }
    private void SaveRecord(){
       int antRec= Integer.parseInt(shared.getString("record",""));

        if(antRec>pCont) {
            SharedPreferences.Editor edit = shared.edit();
            edit.putString("record", "" + pCont);
            edit.commit();
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

}
